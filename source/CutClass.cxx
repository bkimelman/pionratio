#ifndef __CUT_CLASS_CXX__
#define __CUT_CLASS_CXX__

#include "../headers/CutClass.h"

CutClass::CutClass(){
  m_cutOnTriggers = false;
  m_beamXCenter = 0;
  m_beamYCenter = 0;
  m_ZLowCut = -400;
  m_ZHighCut = 400;
  m_radialCut = 100;
  m_DCA = 100;
  m_tofMatchCut = -1; //Greater or Equal To
  
  m_useT0 = false;
  m_nTofT0Cut = -1;
  m_NHitsFit = 0;
  m_NHitsDeDx = 0;
  m_fitMaxRatio = 0.51;
  
  m_btofLocY = 5;
  m_btofLocZ = 5;

  m_pileUpCut = 1e20;
  m_numCentralities = 0;

  
  m_eventConfig = "ColliderCenter";
  m_yCM = 0.0;
  m_sqrts_NN = 19.6;
  m_beamEnergy = 9.8;
  m_starver = "dev";

}

void CutClass::setZRange(double a_zLow, double a_zHigh){
  m_ZLowCut = a_zLow; 
  m_ZHighCut = a_zHigh; 
  //cout << m_ZLowCut << " = " << a_zLow << endl << m_ZHighCut << " = " << a_zHigh << endl;
}

std::pair<double,double> CutClass::getZVertexCuts(){
  std::pair<double,double> zVtx;
  zVtx.first = m_ZLowCut;
  zVtx.second = m_ZHighCut;
  return zVtx;
}

std::pair<double,double> CutClass::getBeamSpotLocation(){
  std::pair<double,double> beamXY;
  beamXY.first = m_beamXCenter;
  beamXY.second = m_beamYCenter;
  return beamXY;
}

/*
void CutClass::setBeamLocation(double a_x, double a_y){
  m_beamXCenter = a_x;
  m_beamYCenter = a_y;
  //cout << m_ZLowCut << " = " << a_zLow << endl << m_ZHighCut << " = " << a_zHigh << endl;
  }*/


void CutClass::setTriggers(int a_numTriggers, unsigned int* a_triggers){
  for(int iii = 0; iii < a_numTriggers; ++iii){
    m_triggers.push_back(a_triggers[iii]);
  }
  return;
}


void CutClass::setHLTTriggers(int a_numHLTTriggers, unsigned int* a_HLTTriggers){
  for(int iii = 0; iii < a_numHLTTriggers; ++iii){
    m_HLTTriggers.push_back(a_HLTTriggers[iii]);
  }
  return;
}
void CutClass::setEToFTriggers(int a_numEToFTriggers, unsigned int* a_EToFTriggers){
  for(int iii = 0; iii < a_numEToFTriggers; ++iii){
    m_EToFTriggers.push_back(a_EToFTriggers[iii]);
  }
  return;
}


void CutClass::setYCM(Float_t a_yCM){
  if (m_eventConfig == "ColliderCenter"){
    m_yCM = 0.0;
    return;
  }
  else if (m_eventConfig == "FixedTarget"){
    if (a_yCM > 0) m_yCM = -a_yCM;
    else m_yCM = a_yCM;
  }
}



void CutClass::setCentralities(int a_numCent, double* a_cent_edges, int* a_cent_percents){
  m_numCentralities = a_numCent;
  m_centPercents.clear();
  m_centEdges.clear();
  m_centPercents.push_back(0.0);
  if(a_cent_edges[0] != 0) m_centEdges.push_back(1000.0*a_cent_edges[0]);
  else m_centEdges.push_back(100000.0);
  for (int iii = 0; iii < a_numCent; iii++){
    m_centPercents.push_back(a_cent_percents[iii]);
    m_centEdges.push_back(a_cent_edges[iii]);
    //    cout << m_centPercents[iii] << " " << m_centEdges[iii] << endl;
  }
}


bool CutClass::isHLTEvent(StPicoEvent* a_event){
  bool HLTfoundInTriggers = false;
  if(m_cutOnTriggers){
    vector <unsigned int> trigIDs = a_event->triggerIds();
    for(int reqTrigIndex = 0; reqTrigIndex < (int)m_HLTTriggers.size(); reqTrigIndex++){
      for(int eventTrigIndex = 0; eventTrigIndex < (int)trigIDs.size(); eventTrigIndex++){
	//    cout << trigIDs[eventTrigIndex] << " to " << m_triggers[reqTrigIndex] << endl;
        if(trigIDs[eventTrigIndex] == m_HLTTriggers[reqTrigIndex]) HLTfoundInTriggers = true;
        if(HLTfoundInTriggers) break; 
      }
      if(HLTfoundInTriggers) break;
    }
  }
  return HLTfoundInTriggers;
}

bool CutClass::isEToFEvent(StPicoEvent* a_event){
  bool EToFfoundInTriggers = false;
  if(m_cutOnTriggers){
    vector <unsigned int> trigIDs = a_event->triggerIds();
    for(int reqTrigIndex = 0; reqTrigIndex < (int)m_EToFTriggers.size(); reqTrigIndex++){
      for(int eventTrigIndex = 0; eventTrigIndex < (int)trigIDs.size(); eventTrigIndex++){
	//    cout << trigIDs[eventTrigIndex] << " to " << m_triggers[reqTrigIndex] << endl;
        if(trigIDs[eventTrigIndex] == m_EToFTriggers[reqTrigIndex]) EToFfoundInTriggers = true;
        if(EToFfoundInTriggers) break; 
      }
      if(EToFfoundInTriggers) break;
    }
  }
  return EToFfoundInTriggers;
}

bool CutClass::isGoodEvent(StPicoEvent* a_event){
  //Vertex
  double z =(double) a_event->primaryVertex().Z();
  double r = TMath::Sqrt( pow(a_event->primaryVertex().X()-m_beamXCenter,2) + pow(a_event->primaryVertex().Y()-m_beamYCenter,2) );

if (z < m_ZLowCut) return false;
if (z > m_ZHighCut) return false;
if (r > m_radialCut) return false;
if (m_tofMatchCut > a_event->nBTOFMatch()) return false;
if(m_useT0){
  if (a_event->nTofT0() < m_nTofT0Cut) return false;   
}
  // Trigger
bool goodEvent = true;
if(m_cutOnTriggers){
  bool foundInTriggers = false;
  vector <unsigned int> trigIDs = a_event->triggerIds(); 
  for(int reqTrigIndex = 0; reqTrigIndex < (int)m_triggers.size(); reqTrigIndex++){
      //foundInTriggers = false;
    for(int eventTrigIndex = 0; eventTrigIndex < (int)trigIDs.size(); eventTrigIndex++){
	//    cout << trigIDs[eventTrigIndex] << " to " << m_triggers[reqTrigIndex] << endl;
      if(trigIDs[eventTrigIndex] == m_triggers[reqTrigIndex]) foundInTriggers = true;
      if(foundInTriggers) break;
    }
    if(foundInTriggers) break;
  }

  if(!foundInTriggers) goodEvent = false;
}

return goodEvent;

}

bool CutClass::isGoodTrack(StPicoTrack* a_track, TVector3* a_EventVertex, bool a_ignoreDCA){  
  if(!(a_track->isPrimary())) return false;
  // cout << "Track is primary" << endl;
  if(  ((double) (a_track->nHitsFit())) / ((double)(a_track->nHitsMax())) < m_fitMaxRatio ) return false;
  //  cout << "Track has good enough hit fraction: " << ((double) (a_track->nHitsFit())) / ((double)(a_track->nHitsMax())) << " > " << m_fitMaxRatio << endl;
  if(a_track->nHitsFit() < m_NHitsFit) return false;
  //  cout << "Track has enough hits: " << a_track->nHitsFit() << " > " << m_NHitsFit << endl;
  if(a_track->nHitsDedx() < m_NHitsDeDx) return false;
  //  cout << "Track has enough dEdx hits: " << a_track->nHitsDedx() << " > " << m_NHitsDeDx << endl;
  if(!a_ignoreDCA && a_track->gDCA(*a_EventVertex).Mag() > m_DCA) return false;
  //  cout << "Track has good enough DCA: " << a_track->gDCA(*a_EventVertex).Mag() << " < " << m_DCA << endl;
  //  cout << "Track is good" << endl;

  return true;
}


bool CutClass::isGoodBTof(StPicoBTofPidTraits* a_traits){
  if(a_traits->btofMatchFlag() == 0) return false;
  if(fabs(a_traits->btofYLocal()) > m_btofLocY) return false;
  if(fabs(a_traits->btofZLocal()) > m_btofLocZ) return false;
  if(a_traits->btofBeta() < 0) return false;
  return true;
}

bool CutClass::isGoodETof(StPicoETofPidTraits* a_traits){
  if(a_traits->matchFlag() == 0) return false;
  //if(fabs(a_traits->btofYLocal()) > m_btofLocY) return false;
  //if(fabs(a_traits->btofZLocal()) > m_btofLocZ) return false;
  if(a_traits->beta() < 0) return false;
  return true;

}



int CutClass::centralityIndex(int a_mult){
  for(int iii = 0; iii < m_numCentralities+1; iii++){
    if(a_mult >= m_centEdges[iii]) return iii-1;
  }
  return -1;
}

bool CutClass::isPileUp(int a_centVariable){
  if( a_centVariable > m_pileUpCut ) return true; 
  else return false;
}




#endif
