#include <iostream>
#include <fstream>
#include <sstream>
#include <cmath>

#include <TF1.h>
#include <TH1D.h>
#include <TH2D.h>
#include <TH3D.h>
#include <TGraphErrors.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TCanvas.h>
#include <TClonesArray.h>
#include <TSystem.h>
#include <TLatex.h>
#include <THistPainter.h>
#include <TAttLine.h>
#include <TLegend.h>
#include <TStyle.h>
#include <TVector3.h>
#include <TMath.h>


//after- verify that you need all these classes
#include "../submodule/PicoDstReader/StPicoDstReader.h"
  #include "../submodule/PicoDstReader/StPicoDst.h"
  #include "../submodule/PicoDstReader/StPicoEvent.h"
  #include "../submodule/PicoDstReader/StPicoTrack.h" 
  #include "../submodule/PicoDstReader/StPicoBTofPidTraits.h"
  #include "../submodule/PicoDstReader/StPicoETofPidTraits.h"

#include "../submodule/ParticleInfo/ParticleInfo/ParticleInfo.h"
#include "../headers/CutClass.h"

using namespace std;

//_____MAIN____________________
void makePionYields(TString inputDataFile, TString outputFile, CutClass *cuts, Int_t nEvents = -1){
//This function takes your input data file and produces an output file with vertex qa plots
//with no vertex cuts or with vertex-level cuts. Note that no track qa plots are made 
//you must use trackQAmaker.cxx for track qa. The purpose of this function is to allow 
//the user to optimize vertex cuts.

//  TFile *zombieTest = new TFile(inputDataFile);
//  if( zombieTest->IsZombie() ) return;

  TString eventConfig = cuts->getEventConfig();
  Float_t yCM = cuts->getYCM();

  cout << "eventConfig: " << eventConfig << endl;
  cout << "yCM: " << yCM << endl;

  StPicoDstReader* picoReader = new StPicoDstReader(inputDataFile);
  picoReader->Init();
  picoReader->SetStatus("*",0);
  picoReader->SetStatus("Event",1);
  picoReader->SetStatus("Track",1);                                          
  picoReader->SetStatus("BTofPidTraits",1);
  picoReader->SetStatus("ETofPidTraits",1);

  if( !picoReader->chain() ) {
   std::cout << "No chain has been found." << std::endl;
 }
 Long64_t eventsInTree = picoReader->tree()->GetEntries();
 //std::cout << "eventsInTree: "  << eventsInTree << std::endl;
 Long64_t events2read;
 if(nEvents != -1) events2read = nEvents;
 else events2read = picoReader->chain()->GetEntries();

 cout << "Total Events Loaded: " << picoReader->chain()->GetEntries() << endl;
 cout << "Events to Read: " << events2read << endl;

 TFile *outFile  = new TFile(outputFile,"RECREATE");

 const int nBins = 42; //was 21

 //histograms
 TH1D *nEventsHist = new TH1D("nEventsHist","nEvents",2,-0.5,1.5);
 TH1D *piPlus = new TH1D("piPlus","Pion+ yield",nBins,0,0.3);//was .5
 TH1D *piMinus = new TH1D("piMinus","Pion- yield",nBins,0,0.3);//was .5
 
 Int_t pvEntries;
 double mPion = 0.13957018;

 bool trackCut=false;

 int modNum=1;

 Int_t multVar = 0;
 
for(Int_t iEvent=0; iEvent<events2read; iEvent++){//loop over triggers

  Bool_t readEvent = picoReader->readPicoEvent(iEvent);

  if( !readEvent ) {
    std::cout << "Something went wrong, no events" << endl;
    break;
  }


  StPicoDst *dst = picoReader->picoDst();

  
  if( iEvent % modNum == 0 ){
    cout << "runByRunQA::Working on event " << iEvent << endl;
    if (iEvent == 10*modNum) modNum *= 10;
  }


  //access data and fill trigger level histograms                                      
  StPicoEvent *event = dst->event();
  if( !event ) {
    cout << "Something went wrong, event is empty" << endl;
    break;
  }


  if (!cuts->isGoodEvent(event)) continue;

  nEventsHist->Fill(1);

  TVector3 primVertex = event->primaryVertex();
  
  Int_t nTracks = dst->numberOfTracks();
  if(cuts->getEventConfig() == "FixedTarget"){
    multVar = 0;
    for(Int_t iTrk = 0; iTrk<nTracks;iTrk++){
      StPicoTrack *picoTrack = dst->track(iTrk);
      if(picoTrack->isPrimary()) multVar++;
    }
  }else{
    multVar = event->refMult();
  }

  if(cuts->isPileUp(multVar)) continue;
  
  int centIndex = cuts->centralityIndex(multVar);
  if (centIndex < 0) continue;
  double centHighPct = cuts->getHighCentPercent(centIndex);

  if(centHighPct > 5) continue;
  
  
  

  
  // Loop to get numbers of primary tracks
  for(Int_t iTrk = 0; iTrk<nTracks;iTrk++){
    StPicoTrack *picoTrack = dst->track(iTrk);

    TVector3 momentum = picoTrack->pMom();
    double mTm0 = sqrt(pow(momentum.Pt(),2)+pow(mPion,2)) - mPion;
    if(mTm0 > 0.3) continue;//was .5

    double rapPion = TMath::ATanH(momentum.Z()/sqrt(pow(momentum.Mag(),2)+pow(mPion,2)));
    if(-(rapPion-yCM) > 0.05 || -(rapPion-yCM) < -0.05) continue;

    if (cuts->isGoodTrack(picoTrack,&primVertex)){    
      trackCut=true;
    }
    else trackCut=false;

    Float_t nSigmaPi = picoTrack->nSigmaPion();
    Short_t q = picoTrack->charge();

    if(trackCut){

      if(q>0 && fabs(nSigmaPi)<2){
	//fill pion plus histogram after this comment	
	piPlus->Fill(mTm0);
      }else if(q<0 && fabs(nSigmaPi)<2){
	//fill pion minus histogram after this comment
	piMinus->Fill(mTm0);
      }

    }//end if primary (trackCut=true)
  } //End track loop
 }//end of loop over events

 piPlus->Sumw2();
 piMinus->Sumw2();

 /*

 double mTm0Arr[nBins];
 double ratioArr[nBins];
 double mTm0Err[nBins];
 double ratioErr[nBins];

 for(int i=0; i<nBins; i++){
   mTm0Arr[i] = piPlus->GetBinCenter(i+1);
   mTm0Err[i] = 0.5*(piPlus->GetBinWidth(i+1));//made changes here. The origional didn't have 0.5*
   ratioArr[i] = 1.0*piPlus->GetBinContent(i+1)/piMinus->GetBinContent(i+1);
   ratioErr[i] = ratioArr[i]*sqrt(pow(piPlus->GetBinError(i+1)/piPlus->GetBinContent(i+1),2)+pow(piMinus->GetBinError(i+1)/piMinus->GetBinContent(i+1),2));
 }

 TGraphErrors *pionRatio = new TGraphErrors(nBins,mTm0Arr,ratioArr,mTm0Err,ratioErr);
 pionRatio->SetName("PionRatio");

 pionRatio->Write("PionRatio");
 */

 piPlus->Write();
 piMinus->Write();
 
 outFile->Write();
 outFile->Close();
}//end of function
