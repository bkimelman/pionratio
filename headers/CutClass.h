#ifndef __CUT_CLASS__
#define __CUT_CLASS__

#include <vector>
#include <string>
#include <iostream>

#include "TMath.h"
//#include "TObject.h"

#include "../submodule/PicoDstReader/StPicoEvent.h"
#include "../submodule/PicoDstReader/StPicoTrack.h"
#include  "../submodule/PicoDstReader/StPicoBTofHit.h"
#include "../submodule/PicoDstReader/StPicoBTofPidTraits.h"
#include "../submodule/PicoDstReader/StPicoETofPidTraits.h"

#include "TH1D.h"

// uncomment the following line and recompile to get debug printouts
//#define _CutClass_Debug_

//class StRefMultCorr;
//class CentralityMaker;

class CutClass{
public:  
  CutClass();
  //~CutClass(){};

  //SETTERS
  void setTriggers(int a_numTriggers, unsigned int* a_triggers);
  void setHLTTriggers(int a_numHLTTriggers, unsigned int* a_HLTTriggers);
  void setEToFTriggers(int a_numEToFTriggers, unsigned int* a_EToFTriggers);

  // assumes 0->_%, so start at 5,10,... then lower bounds for those 350,300,___
  void setTrigToggle(bool a_toggle){m_cutOnTriggers = a_toggle;};

  void setBeamLocation(double a_x, double a_y){m_beamXCenter = a_x; m_beamYCenter = a_y;};
  //  void setBeamLocation(double a_x, double a_y);
  void setZRange(double a_zLow, double a_zHigh);
  void setDCA(double a_dca){m_DCA = a_dca;};
  
  void setRadialCut(double a_rad){m_radialCut = a_rad;};
  void setTofMatch(int a_tofMatches){ m_tofMatchCut = a_tofMatches; };

  void setUseT0(bool a_useT0){ m_useT0 = a_useT0; };
  void setTofT0Cut(int a_nTofT0Cut){ m_nTofT0Cut = a_nTofT0Cut; };

  void setNHitsFit(double a_hits){m_NHitsFit = a_hits;};
  void setNHitsDeDx(double a_hits){m_NHitsDeDx = a_hits;};
  void setFitMaxRatio(double a_ratio){m_fitMaxRatio = a_ratio;};
  void setBTOF(double a_localY, double a_localZ){m_btofLocY = a_localY; m_btofLocZ = a_localZ;};

  void setYCM(Float_t a_yCM);
  void setEventConfig(TString a_eventConfig){m_eventConfig = a_eventConfig;};
  void setStarver(string a_starver){m_starver = a_starver;};
  void setSqrts_NN(Double_t a_sqrts_NN){ m_sqrts_NN = a_sqrts_NN; };
  void setBeamEnergy(Double_t a_beamEnergy){ m_beamEnergy = a_beamEnergy; };


  void setCentralities(int a_numCent, double* a_cent_edges, int* a_cent_percents); //not needed if using StRefMultCorr
  void setPileUpCut(double a_cut=1e150){m_pileUpCut = a_cut;}

  
  bool isHLTEvent(StPicoEvent* a_event);
  bool isGoodEvent(StPicoEvent* a_event);
  bool isEToFEvent(StPicoEvent* a_event);
  bool isGoodETof(StPicoETofPidTraits* a_traits);
  bool isGoodTrack(StPicoTrack* a_track, TVector3* a_EventVertex, bool a_ignoreDCA = false);

  bool isGoodBTof(StPicoBTofPidTraits* a_traits);

  bool isPileUp(int a_centVariable);
  int centralityIndex(int a_mult); //returns -1 if not found
  int getNCentBins(){return m_numCentralities;};
  
  double getLowCentPercent(int a_centIndex){return m_centPercents[a_centIndex];};
  double getHighCentPercent(int a_centIndex){return m_centPercents[a_centIndex+1];};
  double getCentCutLowEdge(int a_centIndex){return m_centEdges.at(a_centIndex+1);};
  double getCentCutHighEdge(int a_centIndex){return m_centEdges.at(a_centIndex);};

  double getLinPileUpCut(){return m_pileUpCut;};

  
  Float_t getYCM(){return m_yCM;};
  TString getEventConfig(){return m_eventConfig;};
  string getStarver(){return m_starver;};
  Double_t getSqrts_NN(){ return m_sqrts_NN; };
  Double_t getBeamEnergy(){ return m_beamEnergy; };
  
  double getDCACut(){ return m_DCA; };
  double getNHitsFitCut(){ return m_NHitsFit; };
  double getNHitsDeDxCut(){ return m_NHitsDeDx; };
  double getFitMaxRatioCut(){ return m_fitMaxRatio; };

  std::pair<double,double> getZVertexCuts();
  double getRadialVertexCut(){ return m_radialCut; };
  std::pair<double,double> getBeamSpotLocation();

  //ClassDef(CutClass, 1);






private:
  
  bool m_cutOnTriggers;

  //vertex
  double m_beamXCenter;
  double m_beamYCenter;
  double m_ZLowCut;
  double m_ZHighCut;
  double m_radialCut;
  double m_DCA;

  double m_pileUpCut;
  int m_numCentralities;
  vector<double> m_centPercents;
  vector<double> m_centEdges;

  
  int m_tofMatchCut; //Greater or Equal To

  bool m_useT0;
  int m_nTofT0Cut;

  vector<unsigned int> m_triggers;
  vector<unsigned int> m_HLTTriggers;
  vector<unsigned int> m_EToFTriggers;

  int m_NHitsFit;
  int m_NHitsDeDx;
  double m_fitMaxRatio;

  double m_btofLocY;
  double m_btofLocZ;
  
  TString m_eventConfig;
  Float_t m_yCM;

  Double_t m_sqrts_NN;
  Double_t m_beamEnergy;
  
  string m_starver;

};

#endif




