#####MAKEFILE######

CXX = g++

# Define flags. -D_VANILLA_ROOT_ is needed to avoid StMessMgr confusion                
CFLAGS = $(shell root-config --cflags) -O2 -fPIC -Wall -pipe -std=c++11 -D_VANILLA_ROOT_ -I.
LIBS = $(shell root-config --libs)
INCS = $(shell root-config --incdir)

# Define output library                                                                
STPICODST = libStPicoDst.so

# Compile all *.cxx classes in the directory                                           
SRC = $(shell find . -name "*.cxx")


BIN_DIR = ./bin


all: directories
	$(MAKE) $(STPICODST) --directory=submodule/PicoDstReader/
	cp submodule/PicoDstReader/libStPicoDst.so ./bin/.
	root -l -q -b macros/makeLibs.C

$(STPICODST): $(SRC:.cxx=.o) StPicoDst_Dict.C
	$(CXX) $(CFLAGS) -shared $^ -o $(STPICODST) $(LIBS)

%.o: %.cxx
	$(CXX) -fPIC $(CFLAGS) -c -o $@ $<

StPicoDst_Dict.C: $(shell find . -name "*.h" ! -name "*LinkDef*")
	rootcint -f $@ -c -D_VANILLA_ROOT_ -DROOT_CINT -D__ROOT__ -I. -I$(INCS) $^ StPicoDstLinkDef.h


clean:
	root -l -q -b macros/makeLibs.C\(\"clean\"\)
#	$(MAKE) $(STPICODST) --directory=submodule/PicoDstReader/
	$(MAKE) clean --directory=submodule/PicoDstReader/
#	rm -vf *.o StarRoot/*.o StPicoDst_Dict* $(STPICODST)
#	rm submodule/PicoDstReader/StPicoDst_Dict.h

directories:
	mkdir -p $(BIN_DIR)

