collision#!/bin/bash
###########################################################
#SET THE DATA FILE LIST HERE

collision=AuAu_7p7GeV_FXT

inFileList=/scratch_star/jnian/pionratio/FastOffline_ListFiles/${collision}_FO_fileList.list

outFile=../userfiles/${collision}_pionYields.root

nEvents=-1

root -l -b -q ../macros/RunMakePionYields.C\(\"$inFileList\",\"$outFile\",\"$collision\",$nEvents\)

exit
