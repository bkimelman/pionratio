#!/bin/bash
###########################################################
#SET THE DATA FILE LIST HERE

energy=7.7

energyStr=`echo "$energy" | tr . p`
collision=AuAu_${energyStr}GeV_FXT

inFile=../userfiles/${collision}_pionYields.root
#/scratch_menkar/bkimelman/3GeV_PionRatio_July20_forJimmy.root
#/scratch_menkar/bkimelman/3GeV_pionYield_forJimmy.root(7/16)


outFile=../userfiles/${collision}_pionRatioFit.png

root -l -b -q ../macros/FitPionRatio.C\($energy,\"$inFile\",\"$outFile\"\)
#root -l -b -q ../macros/FitPionRatio.C

cp ${outFile} /home/jnian/public_html/protected/.
#cp ../userfiles/AuAu_3p0GeV_FXT_BenData_pionRatioFit.png /home/jnian/public_html/protected/.

exit
